# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/02/24 06:29:29 by rbernand          #+#    #+#              #
#    Updated: 2015/08/27 11:35:32 by rbernand         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=42sh
CC=	cc
FLAGS=-Wall -Wextra -O3 -ggdb
LIB=libft/
INCLUDES=includes/
DIROBJ=objs/
DIRSRC=srcs/
DIRBIN=bin/
SRC=cd.c \
	colors.c \
	echo.c \
	env.c \
	env_fct.c \
	error.c \
	exec_and.c \
	exec_cmd.c \
	exec_rlleft.c \
	exec_rleft.c \
	exec_or.c \
	exec_pipe.c \
	exec_rright.c \
	exec_tree.c \
	exec_word.c \
	exit.c \
	fork_fct.c \
	add_cmd_hist.c \
	jobs.c \
	main.c \
	outpout.c \
	outpout_color.c \
	prompt.c \
	setenv.c \
	signal.c \
	singleton.c \
	term_clear_line.c \
	term_complete_file.c \
	term_cursor.c \
	term_eof.c \
	term_tab.c \
	term_full_move.c \
	term_get_line.c \
	term_history.c \
	term_key_delete.c \
	term_move_cursor.c \
	term_move_word.c \
	term_output.c \
	term_termios.c \
	term_winsize.c \
	tools.c \
	unsetenv.c \
	lex_clst.c \
	lex_lex.c \
	lex_pars.c \
	lex_error.c \
	lex_split.c \
	wait.c
OBJ=$(SRC:%.c=$(DIROBJ)%.o)

all: init $(NAME) end

init:
	@tput init
	@make -s -C $(LIB)
	@mkdir -p $(DIRBIN)
	@mkdir -p $(DIROBJ)

end:
	@echo "\033[2K\t\033[1;36m$(NAME)\t\t\033[0;32m[Ready]\033[0m"

$(NAME): $(OBJ)
	@echo "\033[2KCompiling $(NAME) : "
	@$(CC) $(FLAGS) -o $@ $^ -I$(INCLUDES) -L$(LIB) -lft -ltermcap
	@tput cuu1
	@cp $(NAME) $(DIRBIN)

$(DIROBJ)%.o: $(DIRSRC)%.c
	@echo "\r\033[2KCompiling $< : "
	@$(CC) $(FLAGS) -o $@ -c $< -I$(INCLUDES)
	@tput cuu1

clean:
	@rm -f $(OBJ)

fclean: clean
	@rm -rf $(NAME)
	@rm -rf $(DIROBJ)
	@rm -rf $(DIRBIN)

re: fclean all

