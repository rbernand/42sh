/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/18 00:49:16 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/18 02:39:10 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HISTORY_H
# define HISTORY_H

typedef struct s_cmd_hist			t_cmd_hist;
struct								s_cmd_hist
{
	char						*cmd;
	unsigned int				id;
	t_cmd_hist					*next;
	t_cmd_hist					*prev;
};

typedef struct s_history			t_history;
struct								s_history
{
	t_cmd_hist				*start;
	t_cmd_hist				*current;
	t_cmd_hist				*end;
};

extern t_history					g_history;

void								add_cmd_hist(char *cmd);

#endif
