/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_pars.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/10 20:32:31 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/13 13:07:42 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEX_PARS_H
# define LEX_PARS_H
# include <sys/types.h>

typedef enum				e_type
{
	_unset = -1,
	_and,
	_sep,
	_or,
	_right,
	_rright,
	_left,
	_lleft,
	_pipe,
	_word,
	_end
}							t_type;

typedef struct s_tree		t_tree;
typedef struct s_clst		t_clst;

struct				s_clst
{
	char			c;
	t_clst			*next;
};

struct				s_tree
{
	t_type			type;
	t_clst			*content;
	size_t			size;
	char			*str;
	t_tree			*next;
	t_tree			*left;
	t_tree			*right;
};

t_tree				*analyzer(char *cmd);
void				free_tree(t_tree *tree);

t_tree				*split_recursive(t_tree *tok, t_type type);
int					check_error(t_tree *tree);
t_tree				*lex(char *str);
void				add_clst(t_tree *tok, char c);

#endif
