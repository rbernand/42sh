/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termsh.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 13:38:03 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/07 18:32:18 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TERMSH_H
# define TERMSH_H

# define K0					key[0]
# define K1					key[1]
# define K2					key[2]
# define K3					key[3]

# include <sys/types.h>

typedef struct				s_char
{
	char					c;
	char					state;
	struct s_char			*next;
	struct s_char			*prev;
}							t_char;

typedef struct				s_cursor
{
	size_t					line_size;
	size_t					pos_col;
	t_char					*start;
	t_char					*current;
	t_char					*end;
}							t_cursor;

extern t_cursor				g_cursor;
extern struct winsize		g_winsize;
extern struct termios		*g_backup_term;

char						*get_line(int len);
void						clear_line(t_cursor *cursor);

t_cursor					*mem_cursor(t_cursor *cursor);
void						move_cursor(t_cursor *cursor, char key[4]);
void						term_delete(t_cursor *cursor);
void						term_backspace(t_cursor *cursor);
void						term_eof(t_cursor *cursor);
void						term_tab(t_cursor *cursor);
void						term_complete_file(t_cursor *cur, const char *name);
void						term_history(t_cursor *cursor, char key[4]);
void						term_move_word(t_cursor *cursor);
void						term_full_move(t_cursor *cursor, char key[4]);

void						print_lst(t_cursor *cursor);
void						init_cursor(t_cursor *cursor, int len);
void						init_winsize(struct winsize *winsize);
void						insert_char(t_cursor *cursor, char c);
int							init_term(void);
void						exit_term(void);

#endif
