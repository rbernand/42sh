/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 23:22:38 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/07 17:35:41 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_H
# define SHELL_H
# include <lex_pars.h>

/*
** Defintion of kikooshell
*/

# define BLACK		30
# define RED		31
# define GREEN		32
# define YELLOW		33
# define BLUE		34
# define PURPLE		35
# define CYAN		36
# define WHITE		37
# define NORMAL		0
# define BOLD		1
# define UNDERLINE	4
# define RIGHTS		S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH

typedef int				(*t_builtin_fct)(char **);

typedef struct			s_fork
{
	int					pid;
	struct s_fork		*next;
}						t_fork;

typedef struct			s_env
{
	char				*name;
	char				*value;
	struct s_env		*next;
	struct s_env		*prev;
}						t_env;

typedef struct			s_cmd
{
	char				**args;
	int					type;
	struct s_cmd		*next;
}						t_cmd;

/*
**	GLOBALS
*/
extern t_env			*g_env;
extern t_fork			*g_fork;
extern unsigned int		g_last_return;
extern char				g_colors;
extern char				g_interrupt;

/*
**	Builtins
*/
int						echo42(char **args);
int						setenv42(char **args);
int						jobs42(char **args);
int						unsetenv42(char **args);
int						cd(char **args);
int						exit42(char **args);
int						env(char **args);
int						colors(char **args);

/*
**	fork.c
*/
int						my_fork(void);
int						unset_fork(int pid);

/*
**	error.c
*/
int						msg_error(int num, char *str);

/*
**	setenv.c
*/
void					setenv_exec(char *name, char *value, int over);

/*
**	unsetenv.c
*/
void					unsetenv_exec(char *name);

/*
**	env_fct.c
*/
t_env					*init_env(char **envp);
void					put_env(t_env *env);
t_env					*get_var_env(char *name);
char					**get_env(void);

/*
**	output_color.c
*/
int						putchar_col(int c, int color, int state, int fd);
int						putstr_col(char *str, int color, int state, int fd);
int						putendl_col(char *str, int color, int state, int fd);

/*
**	prompt.c
*/
int						prompt(char **line);

/*
**	exec fct
*/
void					exec_tree(t_tree *tree);
void					exec_pipe(t_tree *tree);
void					exec_rleft(t_tree *tree);
void					exec_rlleft(t_tree *tree);
void					exec_rright(t_tree *tree);
void					exec_word(t_tree *tree);
void					exec_cmd(char **args, char *path);
void					exec_and(t_tree *tree);
void					exec_or(t_tree *tree);
char					*get_bin(char *args, char *path);

/*
**	tools.c
*/
char					**tree_to_tab(t_tree *data);
void					*sh_malloc(unsigned int size);

/*
**	wait.c
*/
void					my_wait(int pid);
/*
**	signal.c
*/
void					init_signal(void);
void					sig_int(int n);
#endif
