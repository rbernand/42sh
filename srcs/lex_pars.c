/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/11 19:26:09 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/07 17:42:57 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <lex_pars.h>

static void		set_type(t_tree *tok)
{
	while (tok)
	{
		if (tok->type == _unset && tok->str)
		{
			if (ft_strcmp(tok->str, "&&") == 0)
				tok->type = _and;
			else if (ft_strcmp(tok->str, "||") == 0)
				tok->type = _or;
			else if (ft_strcmp(tok->str, "|") == 0)
				tok->type = _pipe;
			else if (ft_strcmp(tok->str, ">") == 0)
				tok->type = _right;
			else if (ft_strcmp(tok->str, ">>") == 0)
				tok->type = _rright;
			else if (ft_strcmp(tok->str, "<") == 0)
				tok->type = _left;
			else if (ft_strcmp(tok->str, "<<") == 0)
				tok->type = _lleft;
			else
				tok->type = _word;
		}
		tok = tok->next;
	}
}

static void		clst_to_string(t_tree *tok)
{
	size_t			i;
	t_clst			*tofree;
	t_clst			*clst;

	while (tok)
	{
		tok->str = ft_strnew(tok->size);
		clst = tok->content;
		i = 0;
		while (clst)
		{
			tok->str[i] = clst->c;
			i++;
			tofree = clst;
			clst = clst->next;
			free(tofree);
		}
		tok->size = 0;
		tok->content = NULL;
		tok = tok->next;
	}
}

void			free_tree(t_tree *tree)
{
	if (!tree)
		return ;
	if (tree->next)
		free_tree(tree->next);
	if (tree->right)
		free_tree(tree->right);
	if (tree->left)
		free_tree(tree->left);
	if (tree->str)
		free(tree->str);
	free(tree);
}

static void		replace_tild(t_tree *tree)
{
	if (tree->left)
		replace_tild(tree->left);
	if (tree->right)
		replace_tild(tree->right);
	if (tree->next)
		replace_tild(tree->next);
	if (tree->str)
		if (tree->str[0] == '~')
		{
			free(tree->str);
			tree->str = ft_strdup("cououc");
		}
}

t_tree			*analyzer(char *str)
{
	t_tree		*tok;
	t_tree		*tree;

	if ((tok = lex(str)) == NULL)
		return (NULL);
	clst_to_string(tok);
	set_type(tok);
	tree = split_recursive(tok, _sep);
	tree = split_recursive(tree, _and);
	tree = split_recursive(tree, _or);
	tree = split_recursive(tree, _pipe);
	tree = split_recursive(tree, _rright);
	tree = split_recursive(tree, _right);
	tree = split_recursive(tree, _left);
	tree = split_recursive(tree, _lleft);
	replace_tild(tree);
	if (check_error(tree))
	{
		free_tree(tree);
		return (NULL);
	}
	if (tree->str == NULL || ft_strlen(tree->str) == 0)
		return (NULL);
	return (tree);
}
