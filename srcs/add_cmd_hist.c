/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_cmd_hist.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/18 00:56:45 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/18 02:22:56 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <history.h>

static t_cmd_hist			*new_history(char *cmd)
{
	static int				id = 0;
	static t_cmd_hist		*last = NULL;
	t_cmd_hist				*new;

	if ((new = (t_cmd_hist *)malloc(sizeof(t_cmd_hist))) == NULL)
		return (NULL);
	new->cmd = ft_strdup(cmd);
	new->id = id++;
	new->next = NULL;
	new->prev = last;
	last = new;
	return (new);
}

void						add_cmd_hist(char *cmd)
{
	if (g_history.end)
	{
		g_history.end->next = new_history(cmd);
		g_history.end = g_history.end->next;
	}
	else
	{
		g_history.end = new_history(cmd);
		g_history.start = g_history.end;
	}
	g_history.current = NULL;
}
