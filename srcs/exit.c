/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 09:34:20 by rbernand          #+#    #+#             */
/*   Updated: 2015/03/20 20:59:18 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <shell.h>

int				exit42(char **args)
{
	ft_putstr("exit\n");
	if (args && args[0] && args[1])
		exit(ft_atoi(args[1]));
	exit(g_last_return);
	return (1);
}
