/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/06 12:23:07 by rbernand          #+#    #+#             */
/*   Updated: 2015/02/17 22:04:01 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <shell.h>

void		*sh_malloc(unsigned int size)
{
	void		*ret;

	ret = (void *)malloc(size);
	if (!ret)
		msg_error(103, NULL);
	return (ret);
}

char		**tree_to_tab(t_tree *data)
{
	t_tree		*tmp;
	char		**args;
	int			i;

	tmp = data;
	i = 0;
	while (tmp)
	{
		i++;
		tmp = tmp->next;
	}
	args = (char **)sh_malloc(sizeof(char *) * (i + 1));
	i = 0;
	while (data)
	{
		args[i] = ft_strdup(data->str);
		i++;
		data = data->next;
	}
	args[i] = NULL;
	return (args);
}
