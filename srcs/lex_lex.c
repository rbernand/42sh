/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_lex.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/19 18:49:26 by rbernand          #+#    #+#             */
/*   Updated: 2015/02/19 19:30:04 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <lex_pars.h>

static t_tree			*new_token(void)
{
	t_tree				*new;

	new = (t_tree *)malloc(sizeof(t_tree));
	new->next = NULL;
	new->right = NULL;
	new->left = NULL;
	new->str = NULL;
	new->type = _unset;
	new->content = NULL;
	new->size = 0;
	return (new);
}

static char				*cut_quote(t_tree *tok, char *str)
{
	char				prev;

	tok->type = _word;
	prev = *str++;
	while (*str && *(str) != prev)
	{
		add_clst(tok, *str);
		str++;
	}
	str++;
	return (str);
}

static char				*cut_sep(t_tree *tok, char *str)
{
	char			prev;

	tok->type = *str == ';' ? _sep : _unset;
	prev = *str;
	while (prev == *str)
	{
		add_clst(tok, *str);
		str++;
	}
	return (str);
}

static char				*cut_else(t_tree *tok, char *str, const char *sep)
{
	while (*str && !ft_strchr(sep, *str) && !ft_iswhite(*str))
	{
		add_clst(tok, *str);
		str++;
	}
	return (str);
}

t_tree					*lex(char *str)
{
	t_tree				*tokens;
	t_tree				*c_tok;
	static const char	*sep = ";|<>&\"\'";

	tokens = new_token();
	c_tok = tokens;
	while (*str && ft_iswhite(*str))
		str++;
	while (*str)
	{
		if (*str == '\"' || *str == '\'')
			str = cut_quote(c_tok, str);
		else if (ft_strchr(sep, *str))
			str = cut_sep(c_tok, str);
		else
			str = cut_else(c_tok, str, sep);
		while (*str && ft_iswhite(*str))
			str++;
		if (*str)
		{
			c_tok->next = new_token();
			c_tok = c_tok->next;
		}
	}
	return (tokens);
}
