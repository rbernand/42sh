/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/25 17:32:40 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/25 17:45:50 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int			echo42(char **args)
{
	int		i;
	int		(*f)(const char *);

	i = 1;
	if (args && args[0] && args[1] && ft_strequ(args[1], "-n"))
	{
		i++;
		f = &ft_putstr;
	}
	else
		f = &ft_putendl;
	while (args && args[0] && args[i])
	{
		if (!args[i + 1])
			f(args[i]);
		else
			ft_putstr(args[i]);
		i++;
	}
	return (0);
}
