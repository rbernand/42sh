/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_move_word.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/18 02:36:57 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/07 18:04:08 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <termsh.h>

#include <libft.h>
#include <stdio.h>

static void					move_word_right(t_cursor *cursor)
{
	char				dir[4];

	dir[0] = 27;
	dir[1] = 91;
	dir[2] = 67;
	dir[3] = 0;
	move_cursor(cursor, dir);
	while (!ft_iswhite(cursor->current->c))
	{
		move_cursor(cursor, dir);
		if (cursor->start == cursor->current || cursor->end == cursor->current)
			break ;
	}
	while (ft_iswhite(cursor->current->c))
	{
		move_cursor(cursor, dir);
		if (cursor->start == cursor->current || cursor->end == cursor->current)
			break ;
	}
}

static void					move_word_left(t_cursor *cursor)
{
	char				dir[4];

	dir[0] = 27;
	dir[1] = 91;
	dir[2] = 68;
	dir[3] = 0;
	move_cursor(cursor, dir);
	while (ft_iswhite(cursor->current->c))
	{
		move_cursor(cursor, dir);
		if (cursor->start == cursor->current || cursor->end == cursor->current)
			break ;
	}
	while (cursor->current->prev && !ft_iswhite(cursor->current->prev->c))
	{
		move_cursor(cursor, dir);
		if (cursor->start == cursor->current || cursor->end == cursor->current)
			break ;
	}
}

void						term_move_word(t_cursor *cursor)
{
	char					key[4];

	if (read(0, &key, 4) < 0)
		return ;
	if (K0 == 53 && K1 == 68 && K2 == 0 && K3 == 0)
		move_word_left(cursor);
	else if (K0 == 53 && K1 == 67 && K2 == 0 && K3 == 0)
		move_word_right(cursor);
}
