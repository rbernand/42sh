/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   singleton.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/01 17:28:55 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/25 16:23:33 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <termios.h>

struct termios		*term_backup(void)
{
	static struct termios	*term = NULL;

	if (!term)
		term = (struct termios *)malloc(sizeof(struct termios));
	return (term);
}
