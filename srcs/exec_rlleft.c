/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_rlleft.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 12:19:14 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/07 17:38:51 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/wait.h>
#include <unistd.h>
#include <libft.h>
#include <shell.h>
#include <stdlib.h>

static void		exec_child(t_tree *right, int pipefd[2])
{
	char			*line;

	close(pipefd[0]);
	line = NULL;
	while (ft_putstr("heredoc>")
			&& get_next_line(0, &line) > 0
			&& !ft_strequ(line, right->str))
	{
		ft_putendl_fd(line, pipefd[1]);
		ft_strdel(&line);
	}
	close(pipefd[1]);
	exit(g_last_return);
}

static void		exec_father(t_tree *left, int pipefd[2], int pid)
{
	int				back_up;

	close(pipefd[1]);
	back_up = dup(0);
	dup2(pipefd[0], 0);
	my_wait(pid);
	exec_tree(left);
	dup2(back_up, 0);
}

void			exec_rlleft(t_tree *tree)
{
	int				pipefd[2];
	pid_t			pid;

	if (pipe(pipefd) != 0)
		msg_error(102, NULL);
	if ((pid = my_fork()) == 0)
		exec_child(tree->right, pipefd);
	else
		exec_father(tree->left, pipefd, pid);
}
