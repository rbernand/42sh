/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mem_history.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/18 00:50:28 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/18 00:53:16 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <history.h>

t_history			*mem_history(t_history *history)
{
	static t_history			*memory;

	if (history)
		memory = history;
	return (memory);
}
