/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_fct.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/01 15:14:37 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/13 13:08:00 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <shell.h>

static void		up_shlvl(t_env *env)
{
	int		num;

	while (env && !ft_strequ(env->name, "SHLVL"))
		env = env->next;
	if (env)
	{
		num = ft_atoi(env->value);
		free(env->value);
		env->value = ft_itoa(num + 1, 10);
	}
}

char			**get_env(void)
{
	t_env	*env;
	int		i;
	char	**tab;

	i = 0;
	env = g_env;
	while (env)
	{
		i++;
		env = env->next;
	}
	tab = (char **)sh_malloc(sizeof(char *) * (i + 1));
	tab[i] = NULL;
	env = g_env;
	while (i)
	{
		tab[--i] = ft_strcjoin(env->name, env->value, '=');
		env = env->next;
	}
	return (tab);
}

t_env			*init_env(char **envp)
{
	t_env		*env;
	t_env		*new;
	int			i;
	char		*tmp;

	env = NULL;
	i = 0;
	while (envp && envp[i])
		i++;
	while (--i >= 0)
		if ((tmp = ft_strchr(envp[i], '=')))
		{
			new = (t_env *)sh_malloc(sizeof(t_env));
			new->name = ft_strcdup(envp[i], '=');
			new->value = ft_strdup(tmp + 1);
			new->next = env;
			if (env && env->next)
				env->next->prev = env;
			env = new;
		}
	if (env)
		env->prev = NULL;
	up_shlvl(env);
	return (env);
}

void			put_env(t_env *env)
{
	while (env)
	{
		ft_putstr(env->name);
		ft_putchar('=');
		ft_putstr(env->value);
		ft_putchar('\n');
		env = env->next;
	}
}

t_env			*get_var_env(char *name)
{
	t_env		*env;

	env = g_env;
	while (env)
	{
		if (ft_strequ(name, env->name))
			break ;
		env = env->next;
	}
	return (env);
}
