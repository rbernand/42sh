/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_complete_file.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/17 19:48:06 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/17 22:20:38 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <libft.h>
#include <termsh.h>

void			complete_cursor(t_cursor *cursor, const char *name, int len)
{
	int					i;

	i = len;
	while (name[i])
		insert_char(cursor, name[i++]);
}

static DIR		*get_seeking_dir(const char *file)
{
	char			*last;
	char			**arbo;
	int				i;
	char			*path;
	DIR				*out;

	if (ft_strchr(file, '/') == NULL)
		return (opendir("."));
	arbo = ft_strsplit(file, '/');
	last = ft_strdup(arbo[0]);
	i = 0;
	while (arbo[++i])
	{
		path = ft_strcjoin(last, arbo[i], '/');
		if (access(path, F_OK) < 0)
			break ;
		free(last);
		last = path;
	}
	if (path != last)
		free(path);
	ft_tabdel(&arbo);
	out = opendir(last);
	free(last);
	return (out);
}

void			term_complete_file(t_cursor *cursor, const char *file)
{
	struct dirent		*entry;
	DIR					*dir;
	int					len;

	if ((dir = get_seeking_dir(file)) == 0)
		return ;
	if (ft_strrchr(file, '/') != NULL)
		file = ft_strrchr(file, '/') + 1;
	if (file[0] == 0)
	{
		closedir(dir);
		return ;
	}
	len = ft_strlen(file);
	while ((entry = readdir(dir)))
	{
		if (strncmp(file, entry->d_name, len) == 0)
		{
			complete_cursor(cursor, entry->d_name, len);
			if (entry->d_type == DT_DIR && cursor->current->prev->c != '/')
				insert_char(cursor, '/');
			break ;
		}
	}
	closedir(dir);
}
