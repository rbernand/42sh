/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 16:10:28 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/07 16:30:14 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/ioctl.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <libft.h>
#include <history.h>
#include <shell.h>
#include <termsh.h>

t_env				*g_env = NULL;
t_fork				*g_fork = NULL;
t_history			g_history = {0, 0, 0};
unsigned int		g_last_return = 0;
struct termios		*g_backup_term;
struct winsize		g_winsize;
char				g_colors = 1;
char				g_interrupt = 0;

int			main(int argc, char **argv, char **envp)
{
	char		*line;
	t_tree		*cmd;

	g_backup_term = (struct termios *)malloc(sizeof(struct termios));
	g_env = init_env(envp);
	init_winsize(&g_winsize);
	if (!g_env)
		msg_error(1, NULL);
	(void)argc;
	(void)argv;
	init_signal();
	line = NULL;
	while (prompt(&line))
	{
		add_cmd_hist(line);
		if (line && *line)
			cmd = analyzer(line);
		if (line)
			ft_strdel(&line);
		if (cmd)
			exec_tree(cmd);
		if (cmd)
			free_tree(cmd);
		cmd = NULL;
	}
	return (1);
}
