/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/19 19:33:48 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/07 18:28:48 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include <sys/ioctl.h>
#include <libft.h>
#include <shell.h>
#include <termsh.h>

static void			sig_sigwinch(int n)
{
	struct winsize		win;

	(void)n;
	if (ioctl(0, TIOCGWINSZ, &win) == -1)
	{
	}
	g_cursor.line_size = win.ws_col;
}

void				sig_int(int n)
{
	static char				*signame[] =

	{
	"SIGINT",
	"SIGQUIT",
	};
	if (!g_fork)
		return ;
	ft_putstr("\rprocess [");
	ft_putnbr(g_fork->pid);
	ft_putstr("] has been killed by ");
	ft_putstr(signame[n - 2]);
	ft_putendl(".");
}

void				init_signal(void)
{
	signal(SIGWINCH, &sig_sigwinch);
	signal(SIGQUIT, &sig_int);
	signal(SIGINT, &sig_int);
}
