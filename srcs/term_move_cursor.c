/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_cursor.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 15:39:24 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/07 17:04:25 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <sys/ioctl.h>
#include <term.h>
#include <libft.h>
#include <termsh.h>

void			move_cursor(t_cursor *cursor, char key[4])
{
	if (K2 == 67 && cursor->current->next)
	{
		if (cursor->pos_col % g_winsize.ws_col == 0)
			tputs(tgetstr("do", NULL), 1, ft_putchar);
		else
			tputs(tgetstr("nd", NULL), 1, ft_putchar);
		cursor->pos_col += 1;
		cursor->current = cursor->current->next;
	}
	else if (K2 == 68 && cursor->current->prev)
	{
		cursor->pos_col -= 1;
		cursor->current = cursor->current->prev;
		tputs(tgetstr("le", NULL), 1, ft_putchar);
	}
}
