/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_split.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/18 19:12:23 by rbernand          #+#    #+#             */
/*   Updated: 2015/02/19 19:17:19 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <lex_pars.h>

static t_tree			*split_linear(t_tree *tok, t_type type)
{
	t_tree				*start;
	t_tree				*prev;

	if (tok && !tok->next)
		return (tok);
	start = tok;
	prev = NULL;
	while (tok)
	{
		if (tok->type == type)
		{
			if (prev)
				prev->next = NULL;
			if (tok == start)
				tok->left = NULL;
			else
				tok->left = start;
			tok->right = tok->next;
			tok->next = NULL;
			return (tok);
		}
		prev = tok;
		tok = tok->next;
	}
	return (start);
}

static t_tree			*split_redir2(t_tree *prev, t_tree *tok, t_tree *start)
{
	if (!prev)
		tok->next = NULL;
	else
	{
		if (tok->next)
			prev->next = tok->next->next;
		else
			prev->next = NULL;
		tok->right = tok->next;
		if (tok->right)
			tok->right->next = NULL;
		tok->next = NULL;
		tok->left = start;
	}
	return (tok);
}

static t_tree			*split_redir(t_tree *start, t_tree *tok, t_type type)
{
	t_tree			*prev;

	if (tok && !tok->next)
		return (tok);
	prev = NULL;
	while (tok)
	{
		if (tok->type == type)
		{
			tok->right = tok->next;
			if (prev && tok->next)
				prev->next = tok->next->next;
			else
				return (split_redir2(prev, tok, start));
			if (tok->next)
				tok->next->next = NULL;
			tok->next = NULL;
			tok->left = start;
			return (tok);
		}
		prev = tok;
		tok = tok->next;
	}
	return (start);
}

t_tree					*split_recursive(t_tree *tok, t_type type)
{
	if (!tok)
		return (NULL);
	if (type >= _right && type <= _lleft)
		tok = split_redir(tok, tok, type);
	else
		tok = split_linear(tok, type);
	tok->left = split_recursive(tok->left, type);
	tok->right = split_recursive(tok->right, type);
	return (tok);
}
