/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_clear_line.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 16:21:46 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/07 16:36:30 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <termsh.h>

void		clear_line(t_cursor *cursor)
{
	while (cursor->start != cursor->current)
		term_backspace(cursor);
	while (cursor->current != cursor->end)
		term_delete(cursor);
}
