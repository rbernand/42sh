/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_tree.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/06 09:49:16 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/13 12:20:32 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell.h>

static void		exec_sep(t_tree *tree)
{
	exec_tree(tree->left);
	exec_tree(tree->right);
}

void			exec_tree(t_tree *tree)
{
	static void				(*exec[])(t_tree *) = {

	&exec_and,
	&exec_sep,
	&exec_or,
	&exec_rright,
	&exec_rright,
	&exec_rleft,
	&exec_rlleft,
	&exec_pipe,
	&exec_word
	};
	if (!tree)
		return ;
	if (tree->type == _unset)
		return ;
	exec[tree->type](tree);
}
