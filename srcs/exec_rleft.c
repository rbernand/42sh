/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_left.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/19 19:33:28 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/13 11:55:22 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include "shell.h"
#include "libft.h"

static int		error(int ret, char *err, char *file)
{
	ft_putstr_fd("Error: < ", 2);
	ft_putstr_fd(err, 2);
	ft_putendl_fd(file, 2);
	return (ret);
}

static int		open_file(char *file)
{
	int		fd;

	if ((fd = open(file, O_RDONLY)) < 0)
		return (error(0, "couldn't open file: ", file));
	return (fd);
}

void			exec_rleft(t_tree *tree)
{
	int		fd;
	int		tmp_fd;

	if (tree->right->type != _word)
	{
		ft_putendl_fd("Error, multi < not allowed", 2);
		return ;
	}
	if (!(fd = open_file(tree->right->str)))
		return ;
	tmp_fd = dup(0);
	dup2(fd, 0);
	exec_tree(tree->left);
	dup2(tmp_fd, 0);
	close(fd);
}
