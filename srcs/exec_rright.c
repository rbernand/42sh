/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_rright.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/19 19:33:39 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/13 10:21:04 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <shell.h>
#include <libft.h>

static int	error(int ret, char *err, char *file)
{
	ft_putstr_fd("Error: > or >>, ", 2);
	ft_putstr_fd(err, 2);
	ft_putendl_fd(file, 2);
	return (ret);
}

static int	open_file(char *file, t_type type)
{
	int		fd;

	if (type == _rright)
	{
		if ((fd = open(file, O_CREAT | O_APPEND | O_WRONLY, RIGHTS)) < 0)
			return (error(0, "couldn't open file: ", file));
	}
	else if (type == _right)
	{
		if ((fd = open(file, O_CREAT | O_TRUNC | O_WRONLY, RIGHTS)) < 0)
			return (error(0, "couldn't open file: ", file));
	}
	else
		return (-1);
	return (fd);
}

static int	get_fd(t_tree *tree)
{
	int			fd;
	t_tree		*tmp;
	t_type		type;

	tmp = tree->right;
	type = tree->type;
	while (tmp)
	{
		if (!(fd = open_file(tmp->str, type)))
			return (0);
		if (tmp->right)
			close(fd);
		tmp = tmp->right;
	}
	return (fd);
}

void		exec_rright(t_tree *tree)
{
	int		fd;
	int		tmp_fd;

	fd = 1;
	if (!(fd = get_fd(tree)))
		return ;
	tmp_fd = dup(1);
	dup2(fd, 1);
	exec_tree(tree->left);
	dup2(tmp_fd, 1);
	close(fd);
}
