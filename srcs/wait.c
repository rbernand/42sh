/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wait.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/07 17:46:16 by rbernand          #+#    #+#             */
/*   Updated: 2015/03/21 17:24:09 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include <sys/wait.h>
#include <shell.h>

void		my_wait(int pid)
{
	int				status;

	wait(&status);
	if (WIFEXITED(status))
		g_last_return = WEXITSTATUS(status);
	if (WIFSIGNALED(status))
		g_last_return = 128 + WTERMSIG(status);
	if (WIFEXITED(status) || !WIFSTOPPED(status))
		unset_fork(pid);
}
