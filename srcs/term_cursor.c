/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 14:27:07 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/15 14:33:46 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifdef __APPLE__
# include <term.h>
#else
# include <termios.h>
#endif
#include <unistd.h>
#include <stdlib.h>
#include <libft.h>
#include <sys/ioctl.h>
#include <termsh.h>
#include <shell.h>

t_char		*new_char(char c)
{
	t_char		*new;

	new = (t_char *)sh_malloc(sizeof(t_char));
	if (!new)
		msg_error(103, NULL);
	new->c = c;
	new->state = 0;
	new->next = NULL;
	new->prev = NULL;
	return (new);
}

void		init_cursor(t_cursor *cursor, int len)
{
	cursor->start = new_char('\0');
	cursor->current = cursor->start;
	cursor->end = cursor->current;
	cursor->pos_col = len;
}

void		insert_char(t_cursor *cursor, char c)
{
	t_char			*new;
	static char		t = ' ';

	tputs(tgetstr("ce", NULL), 1, ft_putchar);
	new = new_char(c);
	new->next = cursor->current;
	new->prev = cursor->current->prev;
	if (cursor->current == cursor->start)
		cursor->start = new;
	cursor->current->prev = new;
	if (new->prev)
		new->prev->next = new;
	cursor->pos_col += 1;
	if (cursor->pos_col == cursor->line_size - 1)
		tputs(tgetstr("do", NULL), 1, ft_putchar);
	if (c == '\t')
		write(1, &t, 1);
	else if (ft_isascii(c))
		write(1, &c, 1);
	if (cursor->current != cursor->end)
		print_lst(cursor);
}
