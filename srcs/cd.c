/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 17:29:37 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/13 16:53:53 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <libft.h>
#include <shell.h>

void			update_cd(void)
{
	char		*pwd;
	t_env		*old;

	old = get_var_env("PWD");
	pwd = getcwd(NULL, 0);
	if (old)
		setenv_exec("OLDPWD", old->value, 1);
	setenv_exec("PWD", pwd, 1);
	free(pwd);
}

int				cd(char **args)
{
	int			error;
	t_env		*env;

	error = 0;
	if (!args[1] && (env = get_var_env("HOME")))
		error = chdir(env->value);
	else if (!args[1])
		ft_putendl_fd("cd: HOME not set", 2);
	else if (args[2])
		ft_putendl_fd("cd: Too many arguments", 2);
	else if (ft_strequ(args[1], "-") && (env = get_var_env("OLDPWD")) && env)
		error = chdir(env->value);
	else
		error = chdir(args[1]);
	if (error == -1)
		ft_putendl_fd("cd: No such files or directory, or access denied", 2);
	else
		update_cd();
	return (-error);
}
