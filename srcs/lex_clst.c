/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_clst.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/19 18:52:41 by rbernand          #+#    #+#             */
/*   Updated: 2015/02/19 19:31:14 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <lex_pars.h>

t_clst			*new_clst(char c)
{
	t_clst		*new;

	new = (t_clst *)malloc(sizeof(t_clst));
	new->c = c;
	new->next = NULL;
	return (new);
}

void			add_clst(t_tree *tok, char c)
{
	t_clst			*clst;

	tok->size++;
	if (tok->content == NULL)
		tok->content = new_clst(c);
	else
	{
		clst = tok->content;
		while (clst->next)
			clst = clst->next;
		clst->next = new_clst(c);
	}
}
