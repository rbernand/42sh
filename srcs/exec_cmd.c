/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_cmd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 17:08:40 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/13 12:25:55 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <libft.h>
#include <shell.h>

static int		test_access(char *bin)
{
	if (access(bin, F_OK | X_OK) == 0)
		return (1);
	return (0);
}

char			*get_bin(char *args, char *path_env)
{
	char		**path;
	char		*tmp;
	int			i;

	if (args)
	{
		if (args[0] == '.' || args[0] == '/')
			return (args);
		path = ft_strsplit(path_env, ':');
		i = -1;
		while (path && path[++i])
		{
			tmp = ft_strcjoin(path[i], args, '/');
			if (test_access(tmp))
				break ;
			ft_strdel(&tmp);
		}
		if (path)
			ft_tabdel(&path);
		return (tmp);
	}
	else
		return (NULL);
}

void			exec_cmd(char **args, char *path)
{
	char		*bin;

	if (args && args[0])
	{
		if (args[0][0] == '.' || args[0][0] == '/')
		{
			if (test_access(args[0]))
				execve(args[0], args, get_env());
			else
				msg_error(9, args[0]);
			exit(1);
		}
		else
		{
			bin = get_bin(args[0], path);
			if (bin)
				execve(bin, args, get_env());
			msg_error(8, args[0]);
			ft_strdel(&bin);
			exit(1);
		}
	}
}
