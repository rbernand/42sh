/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_history.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/18 01:14:43 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/15 14:32:54 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <termsh.h>
#include <history.h>

static void				clear_full_line(t_cursor *cursor)
{
	while (cursor->start != cursor->current)
		term_backspace(cursor);
	while (cursor->current != cursor->end)
		term_delete(cursor);
}

static void				string_to_char(t_cursor *cursor, char *cmd)
{
	unsigned int				i;

	i = 0;
	while (cmd[i])
		insert_char(cursor, cmd[i++]);
}

void					term_history(t_cursor *cursor, char key[4])
{
	if (!g_history.start)
		return ;
	clear_full_line(cursor);
	if (K2 == 65)
	{
		if (!g_history.current)
			g_history.current = g_history.end;
		else if (g_history.current != g_history.start)
			g_history.current = g_history.current->prev;
		string_to_char(cursor, g_history.current->cmd);
	}
	else if (K2 == 66)
	{
		if (!g_history.current)
			return ;
		g_history.current = g_history.current->next;
		if (g_history.current)
			string_to_char(cursor, g_history.current->cmd);
	}
}
