/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_tab.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/17 19:11:12 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/18 02:24:34 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <termsh.h>

#include <stdio.h>

static int		count_word(t_char *start)
{
	int			nb_word;

	nb_word = 0;
	while (start)
	{
		while (start && ft_iswhite(start->c))
			start = start->next;
		if (start && start->c)
			nb_word++;
		while (start && !ft_iswhite(start->c))
			start = start->next;
	}
	return (nb_word);
}

static char		*get_word_under_cursor(t_char *current, t_char *start)
{
	char			*sout;
	int				len;
	int				i;

	len = 0;
	if (!current->c && current->prev)
		current = current->prev;
	while (current && !ft_iswhite(current->c) && ++len)
		current = current->prev;
	sout = ft_strnew(len);
	if (!current)
		current = start;
	i = -1;
	while (++i < len)
	{
		current = current->next;
		sout[i] = current->c;
	}
	sout[i] = 0;
	return (sout);
}

static void		autocomplete(t_cursor *cursor)
{
	char			*word;

	word = get_word_under_cursor(cursor->current, cursor->start);
	if (count_word(cursor->start) > 1)
		term_complete_file(cursor, word);
	free(word);
}

void			term_tab(t_cursor *cursor)
{
	if (!cursor->current->prev)
		insert_char(cursor, '\t');
	else if (ft_iswhite(cursor->current->prev->c))
		insert_char(cursor, '\t');
	else
		autocomplete(cursor);
}
