/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_line.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 13:42:33 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/15 14:32:42 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include <sys/ioctl.h>
#include <termcap.h>
#include <termios.h>
#include <stdlib.h>
#include <libft.h>
#include <unistd.h>
#include <termsh.h>
#include <shell.h>

t_cursor			g_cursor = {
	.line_size = 0,
	.pos_col = 0,
	.start = NULL,
	.current = NULL,
	.end = NULL};

static void			exec_key(char key[4], t_cursor *cursor)
{
	int			i;

	if (K0 == 27 && K1 == 91 && (K2 == 68 || K2 == 67) && K3 == 0)
		move_cursor(cursor, key);
	else if (K0 == 127 && K1 == 0 && K2 == 0 && K3 == 0)
		term_backspace(cursor);
	else if (K0 == 27 && K1 == 91 && K2 == 51 && K3 == 126)
		term_delete(cursor);
	else if (K0 == 4 && K1 == 0 && K2 == 0 && K3 == 0)
		term_eof(cursor);
	else if (K0 == 9 && K1 == 0 && K2 == 0 && K3 == 0)
		term_tab(cursor);
	else if (K0 == 27 && K1 == 91 && (K2 == 65 || K2 == 66) && K3 == 0)
		term_history(cursor, key);
	else if (K0 == 27 && K1 == 91 && K2 == 49 && K3 == 59)
		term_move_word(cursor);
	else if ((K0 == 1 || K0 == 5) && K1 == 0 && K2 == 0 && K3 == 0)
		term_full_move(cursor, key);
	else if (K0 == 27 && K1 == 91 && (K2 == 70 || K2 == 72) && K3 == 0)
		term_full_move(cursor, key);
	else if (K0 == 16 && K1 == 0 && K2 == 0 && K3 == 0)
		clear_line(cursor);
	else if ((i = 0) == 0)
		while (i < 4 && key[i])
			insert_char(cursor, key[i++]);
}

static void			sig_int_term(int n)
{
	char			key[4];

	(void)n;
	K0 = 10;
	K1 = 0;
	K2 = 2;
	K3 = 3;
	clear_line(&g_cursor);
	ioctl(0, TIOCSTI, key);
}

static char			*convert(t_cursor *cursor)
{
	int			len;
	t_char		*tmp;
	char		*str;

	len = 1;
	tmp = cursor->start;
	while (tmp->next)
	{
		len++;
		tmp = tmp->next;
	}
	str = (char *)sh_malloc(sizeof(char) * len);
	tmp = cursor->start;
	len = 0;
	while (tmp)
	{
		str[len] = tmp->c;
		len++;
		tmp = tmp->next;
	}
	return (str);
}

static void			char_lst_del(t_cursor *cursor)
{
	t_char		*tmp;
	t_char		*todel;

	tmp = cursor->start;
	while (tmp)
	{
		todel = tmp;
		tmp = tmp->next;
		free(todel);
	}
}

char				*get_line(int len_prompt)
{
	char			*line;
	char			key[4];

	ft_bzero(key, 4);
	signal(SIGINT, &sig_int_term);
	g_cursor.pos_col = len_prompt;
	init_cursor(&g_cursor, len_prompt);
	while (read(0, key, 4) != -1
			&& !(K0 == 10 && K1 == 0 && K2 == 0))
	{
		exec_key(key, &g_cursor);
		ft_bzero(key, 4);
	}
	line = convert(&g_cursor);
	char_lst_del(&g_cursor);
	signal(SIGINT, &sig_int);
	return (line);
}
