/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   outpout_color.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/24 08:58:01 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/18 03:25:19 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <libft.h>
#include <shell.h>

int		putchar_col(int c, int color, int state, int fd)
{
	if (!g_colors)
		return (ft_putchar_fd(c, 2));
	ft_putstr_fd("\033[", fd);
	ft_putnbr_fd(state, fd);
	ft_putchar_fd(';', fd);
	ft_putnbr_fd(color, fd);
	ft_putchar_fd('m', fd);
	return (write(fd, &c, 1) + ft_putstr_fd("\033[0m", fd));
}

int		putstr_col(char *str, int color, int state, int fd)
{
	if (!str)
		return (0);
	if (!g_colors)
		return (ft_putstr_fd(str, 2));
	ft_putstr_fd("\033[", fd);
	ft_putnbr_fd(state, fd);
	ft_putchar_fd(';', fd);
	ft_putnbr_fd(color, fd);
	ft_putchar_fd('m', fd);
	return (write(fd, str, ft_strlen(str)) + ft_putstr_fd("\033[0m", fd) - 4);
}

int		putendl_col(char *str, int color, int state, int fd)
{
	if (!str)
		return (0);
	return (putstr_col(str, color, state, fd) + ft_putchar_fd('\n', fd));
}
