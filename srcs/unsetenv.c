/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unsetenv.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 20:05:45 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 19:20:03 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <shell.h>

int			unsetenv42(char **args)
{
	int			i;

	i = 0;
	while (args && args[i])
		i++;
	if (i == 1)
		msg_error(4, NULL);
	else
	{
		i = 1;
		while (args[i])
			unsetenv_exec(args[i++]);
	}
	return (1);
}

void		unsetenv_exec(char *name)
{
	t_env		*env;

	env = get_var_env(name);
	if (env && env == g_env)
	{
		free(g_env->name);
		free(g_env->value);
		env = g_env->next;
		free(g_env);
		g_env = env;
	}
	else if (env)
	{
		free(env->name);
		free(env->value);
		env->prev->next = env->next;
		if (env->next)
			env->next->prev = env->prev;
		free(env);
	}
	else
		msg_error(2, name);
}
