/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_eof.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 20:41:19 by rbernand          #+#    #+#             */
/*   Updated: 2015/04/17 19:39:25 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/ioctl.h>
#include <termsh.h>
#include <shell.h>

void		term_eof(t_cursor *cursor)
{
	static char	key[4] =

	{
	4,
	0,
	0,
	0,
	};
	if (cursor->start == cursor->end)
	{
		if (g_fork)
			ioctl(0, TIOCSTI, key);
		else
			exit42(0);
	}
	else
		term_delete(cursor);
}
