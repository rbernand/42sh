/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   jobs.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/25 20:32:13 by rbernand          #+#    #+#             */
/*   Updated: 2015/02/19 19:34:22 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <shell.h>

int		jobs42(char **args)
{
	t_fork		*tmp;

	(void)args;
	tmp = g_fork;
	while (tmp)
		tmp = tmp->next;
	return (0);
}
