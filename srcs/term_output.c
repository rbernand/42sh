/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_output.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 18:03:53 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/07 19:51:33 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <term.h>
#include <unistd.h>
#include <libft.h>
#include <sys/ioctl.h>
#include <termsh.h>

void		print_lst(t_cursor *cursor)
{
	int				n_line;
	t_char			*tmp;
	static char		t = ' ';
	int				a;

	tmp = cursor->current;
	n_line = 0;
	(void)a;
	tputs(tgetstr("sc", NULL), 1, ft_putchar);
	while (tmp)
	{
		if (tmp->c == '\t')
			a = write(1, &t, 1);
		else
			a = write(1, &(tmp->c), 1);
		tmp = tmp->next;
		if (cursor->pos_col % (int)g_winsize.ws_col == 0)
			n_line++;
	}
	while (n_line--)
		tputs(tgetstr("up", NULL), 1, ft_putchar);
	tputs(tgetstr("rc", NULL), 1, ft_putchar);
}
