/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_full_move.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/18 03:31:37 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/15 13:58:16 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <termsh.h>
#include <libft.h>

void				term_full_move(t_cursor *cursor, char key[4])
{
	char					dir[4];

	dir[0] = 27;
	dir[1] = 91;
	dir[3] = 0;
	if (K0 == 1 || K0 == 5)
		dir[2] = K0 == 1 ? 68 : 67;
	else
		dir[2] = K2 == 72 ? 68 : 67;
	move_cursor(cursor, dir);
	while (cursor->start != cursor->current && cursor->end != cursor->current)
		move_cursor(cursor, dir);
}
