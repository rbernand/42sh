/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_error.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/18 19:11:17 by rbernand          #+#    #+#             */
/*   Updated: 2015/02/19 19:26:37 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lex_pars.h>
#include <libft.h>

static int				check_redir(t_tree *tok)
{
	if (!tok->right || !tok->left)
		return (ft_putstr("parse error near: ") + ft_putendl(tok->str));
	if (tok->right->next)
		return (ft_putstr("parse error near: ") + ft_putendl(tok->str));
	return (0);
}

static int				check_left_right(t_tree *tok)
{
	if (!tok->right || !tok->left)
		return (ft_putstr("parse error near: ") + ft_putendl(tok->str));
	return (0);
}

static int				check_null_null(t_tree *tree)
{
	if (tree->left || tree->right)
		return (ft_putstr("parse error near: ") + ft_putendl(tree->str));
	return (0);
}

static int				check_void_void(t_tree *tree)
{
	(void)tree;
	return (0);
}

int						check_error(t_tree *tree)
{
	static int			(*fct_check[])(t_tree *) =

	{
	&check_left_right,
	&check_void_void,
	&check_left_right,
	&check_redir,
	&check_redir,
	&check_redir,
	&check_redir,
	&check_left_right,
	&check_null_null,
	};
	if (fct_check[tree->type](tree) != 0)
		return (1);
	if (tree->left && check_error(tree->left) != 0)
		return (1);
	if (tree->right && check_error(tree->right) != 0)
		return (1);
	return (0);
}
